from flask import Flask, request,jsonify,abort

import pandas as pd
import category_prediction

from nlu_retail import nlu_server 

app = Flask(__name__)



@app.route('/brand_query', methods=['POST'])
def predict_brand():
    """
    Given a query and a url for brand name  it extracts the brand name  in the query
    Keyword arguments:
        query  - particular query for which we need to predict .
    
    """   
    if not request.json:
        abort(400)
    elif not 'text' in request.json:
        message = "Please specify text"
    else:
        data = request.json['text']
        data = [str(txt).upper() for txt in data]
        print "data",data
    if 'url' in request.json:
        url = request.json['url']
    else:
        url = "http://localhost:5005/parse"
    try:  
        print "entering try"
        message = {}
        for i in range(len(data)):    
            print "data" + str(i) + data[i]             
       	    message[data[i]]  = nlu_server(text=data[i],url=url,project="brand_name",model="brand_name")            
        print "message",message
    except:
        message = {}
        message['text'] = "Unable to parse text"
        message['type'] = 0
    return jsonify({'Response': message}), 201  

@app.route('/quantity_query', methods=['POST'])
def predict_quantity():
    """
    Given a query and a url for package quantity  it extracts the package quantity  in the query
    Keyword arguments:
        query  - particular query for which we need to predict .
    
    """   
    if not request.json:
        abort(400)
    elif not 'text' in request.json:
        message = "Please specify text"
    else:
        data = request.json['text']
        data = [str(txt).upper() for txt in data]
    if 'url' in request.json:
        url = request.json['url']
    else:
        url = "http://localhost:5004/parse"
    try: 
        message = {} 
        for i in range(len(data)):  
            print "data" + str(i) + data[i]                
            message[data[i]] = nlu_server(text=data[i],url=url,project="quantity",model="quantity")              
    except:
        message = {}
        message['text'] = "Unable to parse text"
        message['type'] = 0
    return jsonify({'Response': message}), 201  

@app.route('/category_query', methods=['POST'])
def predict_category():
    print "print  entering function"
    if not request.json:
          abort(400)
    elif not 'text' in request.json:
          message = "Please specify text"
    else:
        data = request.json['text']
        print "data",data
        data = [str(txt).upper() for txt in data]
        print "data sending to predict",data
        try:
           print "entering try"
           labels = category_prediction.predict_category(data)
           print "labels from keras",labels
           message = dict(zip(data,labels))
        except:
           message = {}
           message['text'] = "Unable to parse text"
           message['type'] = 0

    return jsonify({'Response':message}), 201 
    
@app.route('/predict_all', methods=['POST'])
def predict_all():
    """
    Given a query and a url for package quantity  it extracts the package quantity  in the query
    Keyword arguments:
        query  - particular query for which we need to predict .
    
    """   
    file_name = False
    if not request.json:
        abort(400)
    elif not 'text' in request.json:
        message = "Please specify text"
    elif 'file' not in request.json:
        data = request.json['text']
        data = [str(txt).upper() for txt in data]
    else:
        file_name = request.json['file']
        data = pd.read_csv("file_name")["text"]
        data = [str(txt).upper() for txt in data]
    if 'url_brand' in request.json:
        url_brand = request.json['url_brand']
    else:
        url_brand = "http://localhost:5005/parse"
    if 'url_quantity' in request.json:
        url_quantity = request.json['url_quantity']
    else:
        url_quantity = "http://localhost:5004/parse"
    message = {} 
    try:
        print "predicting categories"
        categories = category_prediction.predict_category(data)
        print "categories",categories
    except:
        categories = ['Unable to Predict'] * len(data)
        
    try:                
        print  "predicting  Quantities"   
        quantities = [nlu_server(text=dat,url=url_quantity,project="quantity",model="quantity")["PAK SIZE UNIT"][0] for dat in data]
    except:
        quantities = ['Unable to Predict'] * len(data)
    try:
        print  "predicting  Brand Names"   
        brands  = [nlu_server(text=dat,url=url_brand,project="brand_name",model="brand_name")["BRAND NAME"][0] for dat in data]
    except:
        brands = ['Unable to Predict'] * len(data)

    message = [{'text':d,'category': c, 'Brand Name': b,'Quantity':q} for  d, c, b, q in zip(data,categories,brands,quantities)]
    if file_name:
        pd.DataFrame.from_records(message).to_csv("Result_" + file_name)

    return jsonify({'Response': message}), 201  


if __name__ == '__main__':
   app.run(debug = True,port = 5006)  
     
     
     
     
