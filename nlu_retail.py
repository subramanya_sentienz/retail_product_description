import requests



def nlu_server(text,project=None,model=None,url ="http://localhost:5004/parse"):
    """ calls rasa server """ 
    if project and model:
       data = "{" + "\"q\"" + ": "+ "\"" + text + "\"" + "," + "\"project\"" + ":" + "\"" + project + "\"" + "," +  "\"model\"" + ":" + "\"" + model + "\"" + "}"
    elif project:
       data = "{" + "\"q\"" + ": "+ "\"" + text + "\"" + "," + "\"project\"" + ":" + "\"" + project + "\"" + "}"
    elif model: 
        data = "{" + "\"q\"" + ": "+ "\"" + text + "\"" + "," + "\"model\"" + ":" + "\"" + model + "\"" + "}"
    else:
        data = "{" + "\"q\"" + ": "+ "\"" + text + "\"" + "}"
    print "data",data
    r = requests.post(url, data = data)
    if r.status_code == 200:
        return response(r)
    else:
        res = {}
        #res["intent"] = "error during extraction"
        return res

def response(r):
    r = r.json()
    res = {} 
    #res["intent_ranking"] = [(d["name"].encode("utf-8","ignore"),(d["confidence"],3)*100) for d in r["intent_ranking"]]
    for i in range(len(r["entities"])):
        res[r["entities"][i]["entity"].encode("utf-8","ignore")] = []
    for i in range(len(r["entities"])):
        res[r["entities"][i]["entity"]].append(r["entities"][i]["value"].encode("utf-8","ignore").upper())
    return res    